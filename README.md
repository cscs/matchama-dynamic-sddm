# Matchama-Dynamic-SDDM

Dynamic and highly customizable theme built from scratch for [SDDM display manager](https://github.com/sddm/sddm) with [QtQuick 2](https://doc.qt.io/qt-5/qtquick-index.html) (Qt 5.7 >=).<br>
It is compatible with and without [KDE Plasma](https://kde.org/plasma-desktop) and does not depend on KDE Framework.

<p align="center">
<img src="https://gitlab.com/cscs/matchama-dynamic-sddm/raw/master/src/matchama/preview.jpg"
</p>

<h2>Features</h2>

- Pure cross platform Qt-Quick 2.7+.
- [Multiples layouts](medias/docs/layouts.jpg) :framed_picture: _(minimized, icon, tiny, half, fullscreen)_.
- Menu is dynamic and can be moved between the different positions with the mouse.
- Menu support left and right screen sides (mirrored).
- Userlist page with avatars.
- Login page via typing username.
- Session dropdown menu. _(KDE Plasma, Gnome, Lxde...)_.
- Keyboard layout dropdown menu _(English, French, Spanish...)_.
- Password textfield has a clean and a reveal button.
- Username textfield has a clean button.
- Keyboard navigation.
- Compatible with Qt Virtual Keyboard.
- Qt Virtual Keyboard is movable.
- Compatible with KDE Plasma battery widget.
- Support multilingual (with the default SDDM component).
- Support multi-screen: Theme can be enable on all screen or only on the primary screen.
- Screensaver.
- Clock / Date.
- The background / wallpaper support picture and color.
- Wallpaper Blur effect _(on/off & intensity)_.
- System buttons are configurable (ex: enable-disable suspend, reboot, shutdown.. buttons).
- Extremely configurable without need to edit .qml files (see: [Configuration and customisation](#-configuration-and-customisation)).
- Compatible with KDE Plasma config interface (background change with sddm-kcm).

<br>

<h2>Dependencies</h2>

> On KDE Plasma desktop you normally have nothing more to install. Here are some dependencies for other environments or just in case:

* qt5-quickcontrols2
* qt5-graphicaleffects
* qt5-svg

<br>

## Themes and installation

### Matchama Dynamic SDDM Installer

Use the script to install the latest version directly from this repo (independently on your distro):

**NOTE:** Use the same script to update Matchama-Dynamic-SDDM.

#### Download &  Make Excecutable

```shell
curl -O https://gitlab.com/cscs/matchama-dynamic-sddm/raw/master/install.sh
chmod +x install.sh
```

#### Install

```shell
./install.sh
```

#### Uninstall

```shell
uninstall=true ./install.sh
```

#### Manual install :
Download the tar archive from [Source](https://gitlab.com/cscs/matchama-dynamic-sddm) and extract the content, and copy theme from the `src` folder to the theme directory of SDDM.<br>
Example
```shell
sudo tar -xzvf ~/Downloads/matchama-dynamic-sddm.tar.gz 
sudo cp -r matchama-dynamic-sddm/src/matchama /usr/share/sddm/themes
```
After that you may have to point SDDM to the new theme by editing its config files :
```shell
sudoedit /etc/sddm.conf.d/sddm.conf
```
In the `[Theme]` section set `Current=matchama`. For a more detailed description please refer to the [Arch wiki](https://wiki.archlinux.org/index.php/SDDM) on sddm. Note that, depending on your system setup, a duplicate configuration may exist in `/etc/sddm.conf`. Usually this takes preference so you want to set the above line in this file if you have it.

<br>

<h2>Configuration and customisation</h2>

#### KDE Plasma :
The background can be changed to anything you want on the right hand side in<br> 
`System Settings › Startup and Shutdown › Login Screen (SDDM)`<br>
Select the large green button next to `Background:`

> Tip : KDE Plasma provides a command to directly access to SDDM system settings : `kcmshell5 kcm_sddm`.

#### Configuration File :
The theme is extremely customizable by editing its included [theme.conf](../src/matcha/theme.conf) file.<br>
You can easy choose the default interface view, show or hide buttons _(shutdown, reboot, suspend, switch user...), move elements positions..._

The full style can be modified with the config file only : 

- Modify Screensaver timer.
- Enable or not the theme only on the primary display screen.
- Select round or rectangle user icons.
- Modify sizes, borders, colors and opacity of each elements _(menu, background, textbox, buttons, combobox..)_.
- Modify texts format, fonts and colors.
- The differents "status" of each elements is supported : _normal, hover, pressed, focus_.
- All colors can use alpha value "#AARRGGBB".
- Shadows.
- ...

The file can be edited after installation by editing it within the theme folder. For example:
```shell
sudoedit /usr/share/sddm/themes/matcha/theme.conf
```

> Tip : You can test (preview) any SDDM theme if needed without logging out with :<br>
> `sddm-greeter --test-mode --theme /usr/share/sddm/themes/THEMENAME`

<br>

<h2>Troubleshooting</h2>

#### ● No Virtual Keyboard
Make sure to set the following line in `/etc/sddm.conf` under the `[General]` section.

```sh
InputMethod=qtvirtualkeyboard
```

#### ● No User Icon
SDDM reads user icon from either `~/.face.icon` or `FacesDir/username.face.icon`

You need to make sure that SDDM user have permissions to read those files.
In case you don't want to allow other users to access your $HOME you can use
ACLs if your filesystem does support it.

```sh
setfacl -m u:sddm:x /home/username
setfacl -m u:sddm:r /home/username/.face.icon
```

If the first solution does not seem to work, you can place the user icon at the default SDDM location :

```sh
folder path : /usr/share/sddm/faces
icon format : USERNAME.face.icon
```
<br>

#### ● My Keyboard layouts are not visible in the selectbox

There is a "cosmetic" bug with sddm who use xcb API to list the keymaps :<br>
On the initial loading, the keyboard layout dropdown menu shows "English (US)" only (your true keyboard layout is selected but this may be visual wrong).
As soon as any key is pressed then XCB_XKB_NEW_KEYBOARD_NOTIFY event will be issued and then it will return correct layout.<br>
This "bug" is Independent of the theme but this theme is now coded so that selected keyboard layout real name is now automatic returned after a first key is pressed.


- [SDDM Issue - Keyboard layout not detected](https://github.com/sddm/sddm/issues/202)
- [Arch wiki - Xorg Keyboard configuration](https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration)

<br>

### Donate  

Everything is free, but you can donate using these:  

<a href='https://ko-fi.com/X8X0VXZU' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a> &nbsp; <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52'><img height='36' style='border:0px;height:36px;' src='https://gitlab.com/cscs/resources/raw/master/paypalkofi.png' border='0' alt='Donate with Paypal' />  

<br>

<h2>License</h2>

Source code of is licensed under GNU GPL version 3. <br>
QML files are MIT licensed and images are CC BY 3.0.

<br>

<h2>Credits</h2>

- KDE Plasma - [icons](https://github.com/KDE/breeze-icons) _(Breeze)_.
- Arc themes - [icons](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) _(PapirusDevelopmentTeam)_.
- Adapta themes - [icons](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) and [wallpaper](https://github.com/PapirusDevelopmentTeam/adapta-kde) _(PapirusDevelopmentTeam)_.
- Dynamic themes - https://github.com/Rokin05/SDDM-Themes

<br>