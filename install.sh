#!/bin/sh

set -e

gh_repo="matchama-dynamic-sddm"
gh_desc="Matchama Dynamic SDDM"

cat <<- EOF



M    M          t         h                  DDDD                                            SSSS  DDDD   DDDD   M    M  
MM  MM   aaa    t    cc   h       aaa        D   D  y  y  nnn    aaa    m m   i   cc        S      D   D  D   D  MM  MM  
M MM M      a  ttt  c  c  hhhh       a       D   D  y  y  n  n      a  m m m     c  c        SSS   D   D  D   D  M MM M  
M    M   aaaa   t   c     h   h   aaaa  ---  D   D   yyy  n  n   aaaa  m m m  i  c     ---      S  D   D  D   D  M    M  
M    M  a   a   t   c  c  h   h  a   a       D   D     y  n  n  a   a  m m m  i  c  c           S  D   D  D   D  M    M  
M    M   aaaa   tt   cc   h   h   aaaa       DDDD      y  n  n   aaaa  m m m  i   cc        SSSS   DDDD   DDDD   M    M  
                                                    yyy                                                                                              


  $gh_desc
  https://gitlab.com/cscs/$gh_repo


EOF

: "${PREFIX:=/usr/share/sddm}"
: "${TAG:=master}"
: "${uninstall:=false}"

_msg() {
    echo "=>" "$@" >&2
}

_rm() {
    # removes parent directories if empty
    sudo rm -rf "$1"
    sudo rmdir -p "$(dirname "$1")" 2>/dev/null || true
}

_download() {
    _msg "Getting the latest version from GitLab ..."
    wget -O "$temp_file" \
        "https://gitlab.com/cscs/$gh_repo/-/archive/master/matchama-dynamic-sddm-master.tar.gz"
    _msg "Unpacking matchama archive ..."
    tar -xzf "$temp_file" -C "$temp_dir"
}

_uninstall() {
    _msg "Deleting $gh_desc ..."
	_rm "$PREFIX/themes/matchama"
}

_install() {
    _msg "Installing ..."
    sudo cp -R \
        "$temp_dir/$gh_repo-$TAG/src/matchama" \
        "$PREFIX/themes"
}

_cleanup() {
    _msg "Clearing cache ..."
    rm -rf "$temp_file" "$temp_dir" \
        ~/.cache/plasma-svgelements-Matchama* \
        ~/.cache/plasma_theme_Matchama*.kcache
    _msg "Done!"
}

trap _cleanup EXIT HUP INT TERM

temp_file="$(mktemp -u)"
temp_dir="$(mktemp -d)"

if [ "$uninstall" = "false" ]; then
    _download
    _uninstall
    _install
else
    _uninstall
fi